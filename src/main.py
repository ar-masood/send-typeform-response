from Models.Datetime import Datetime
from Models.TypeformWebClient import TypeformWebClient
from Models.Logger import Logger

if __name__ == '__main__':
    dt = Datetime()

    typeform = TypeformWebClient()

    # now datetime
    now = dt.get_now_datetime()

    # seconds to subtract from now
    sec = 10

    # get all responses from the accessibility form that were completed
    # within the past 10 seconds
    payload = {
        'completed': 'true',
        # 'since': dt.typeform_format(dt.subtract_seconds(now, sec))
    }
    response_json = typeform.get_form_responses(typeform.get_accessibility_form_id(), **payload)

    # get list of answers of different people
    form_responses = response_json['items']

    # get all questions from form
    questions = typeform.get_form_questions(typeform.get_accessibility_form_id())

    for response in form_responses:
        # open pdf writer
        for answer in response['answers']:
            for question in questions:
                if answer['field']['id'] == question['id']:
                    # get answer text
                    answer_type = answer['type']
                    answer_text = answer[answer_type]

                    # get question text
                    question_text = question['title']

                    # print(question_text)
                    # print(answer_type)
                    # print(answer_text)
                    # print()
        # send email
        print('email sent')
