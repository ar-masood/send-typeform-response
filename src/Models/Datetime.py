from datetime import datetime, date, timedelta


class Datetime:

    @staticmethod
    def get_now_datetime():
        return datetime.now()

    @staticmethod
    def get_now_date():
        return date.now()

    @staticmethod
    def subtract_seconds(now_datetime, seconds):
        return now_datetime - timedelta(seconds=seconds)

    @staticmethod
    def typeform_format(datetime_obj):
        # 2017-07-10T00:00:00 format
        datetime_string = datetime_obj.strftime("%Y-%m-%dT%H:%M:%S")
        return datetime_string
