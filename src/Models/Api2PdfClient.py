from api2pdf import Api2Pdf


class Api2PdfClient:

    def __init__(self, api_key):
        self.connection = Api2Pdf(api_key)

    def convert_html(self, html):
        api_response = self.connection.HeadlessChrome.convert_from_html(html)
        return api_response.result
