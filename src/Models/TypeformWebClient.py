import requests


class TypeformWebClient:

    def __init__(self):
        auth_token = 'D5PdDbmv4cfwawMrmvHC9TgobdFiTsriu8bhXEduDTJm'
        self.hed = {'Authorization': 'Bearer ' + auth_token}

    def get_account_holder_info(self):
        request_url = 'https://api.typeform.com/me'
        response = requests.get(request_url, headers=self.hed)
        return response.json()

    def get_form_responses(self, form_id, **payload):
        request_url = f'https://api.typeform.com/forms/{form_id}/responses'
        response = requests.get(request_url, headers=self.hed, params=payload)
        return response.json()

    def get_all_forms(self):
        request_url = 'https://api.typeform.com/forms'
        response = requests.get(request_url, headers=self.hed)
        return response.json()

    def get_form(self, form_id):
        request_url = f'https://api.typeform.com/forms/{form_id}'
        response = requests.get(request_url, headers=self.hed)
        return response.json()

    def get_form_questions(self, form_id):
        request_url = f'https://api.typeform.com/forms/{form_id}'
        response = requests.get(request_url, headers=self.hed).json()
        return response['fields']

    @staticmethod
    def get_accessibility_form_id():
        return 'Oj8Zbu'

    @staticmethod
    def get_advanced_care_form_id():
        return 'mVvQrn'
