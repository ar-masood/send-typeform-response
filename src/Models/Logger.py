import logging


class Logger:

    def __init__(self, file_name):
        # Create and configure logger
        logging.basicConfig(
            filename=f'./logs/{file_name}.log',
            format='%(asctime)s %(message)s',
            filemode='w'
        )

        self.logger = logging.getLogger()

    def get_logger(self):
        return self.logger

    def info(self, msg):
        self.logger.info(msg)

    def debug(self, msg):
        self.logger.debug(msg)

    def warning(self, msg):
        self.logger.warning(msg)

    def error(self, msg):
        self.logger.error(msg)

    def critical(self, msg):
        self.logger.critical(msg)

    def exception(self, msg):
        self.logger.exception(msg)
